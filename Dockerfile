From python:3.10.6-slim as base

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONFAULTHANDLER 1

From base as build

COPY Pipfile .

COPY Pipfile.lock .

RUN pip install pipenv

RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --deploy

From base as prod

COPY --from=build /.venv /.venv
ENV PATH="/.venv/bin:$PATH"

# Create and switch to a new user
RUN useradd --create-home appuser
WORKDIR /home/appuser
USER appuser

# Install application into container
COPY . .

ENTRYPOINT [ "python" , "-m" ]

CMD [ "main" ]
